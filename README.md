# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Assignment Demonstration ###
* https://www.youtube.com/watch?v=b1XO6ev3qcs

### Architecture followed is MVVM ###

* This assignment running using MVVM
* Flutter is used as UI framework to develop the app

### Screens completed ###

* Login + Sign up screen
* Login Screen
* Sign up screen
* Bottom tab bar 
* Product List Screen
* Favorite Screen
* Cart Screen

### Third Party Packages Used ###
* http: ^0.13.3
* flutter_spinkit: ^5.0.0
* shared_preferences: ^0.5.8
* provider: ^6.0.1
* form_field_validator: ^1.0.1
