import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/components/button_widget.dart';
import 'package:flutter_coding_challenge/utils/app_constants.dart';
import 'package:flutter_coding_challenge/utils/navigation_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginSignupScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          color: Colors.black,
          child: Column(
            children: [
              SizedBox(height: 80),
              ClipOval(
                child: Container(
                  child: Image.asset("images/rest1.jpeg"),
                  height: 200, //MediaQuery.of(context).size.height / 2,
                  width: 200, //MediaQuery.of(context).size.width / 2,
                ),
              ),
              SizedBox(height: 40),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  CustomButton("Signup", 48, 30.0, 1.0, Colors.blueAccent,
                      Colors.white, Colors.blueAccent, () async {
                        // final prefs = await SharedPreferences.getInstance();
                        // prefs.setBool(ALREADY_LOGIN, false);
                        navigateToSignupScreen(context);
                      }),
                  CustomButton("Login", 48, 30.0, 1.0, Colors.white, Colors.black,
                      Colors.blue, () async {
                        // final prefs = await SharedPreferences.getInstance();
                        // prefs.setBool(ALREADY_LOGIN, true);
                        // navigateToHomeScreen(context);
                        navigateToLoginScreen(context);
                      }),
                  CustomButton("Continue as Guest!", 48, 30.0, 1.0, Colors.transparent,
                      Colors.blueAccent, Colors.transparent, () {
                        navigateToHomeScreen(context);
                      }),
                ],
              )
            ],
          ),
        )
    );
  }
}