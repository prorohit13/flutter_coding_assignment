
import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/utils/app_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginSingupViewModel extends ChangeNotifier {
  void saveUsersLoginToSharedPreference(bool isLoggedIn) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(ALREADY_LOGIN, isLoggedIn);
  }

  Future<bool?> getUsersLoginFromSharedPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool login = prefs.getBool(ALREADY_LOGIN);
    return login;
  }

}