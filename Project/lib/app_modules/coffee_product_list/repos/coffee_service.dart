import 'dart:io';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/repos/api_status.dart';
import 'package:flutter_coding_challenge/utils/app_constants.dart';
import 'package:http/http.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/model/coffee_list.dart';

class CoffeeService {

  static Future<Object?> getCoffeeList() async {
    try {
    Uri url = Uri.parse(COFFEE_LIST_URL);
    Response response = await get(url);
    if (200 == response.statusCode) {
      return Success(code: response.statusCode, response: coffeeFromJson(response.body));
    }
    return Failure(code: INVALID_RESPONSE, errorResponse: "Invalid Response");
    } on HttpException {
      return Failure(code: NO_INTERNET, errorResponse: "No Internet connection");
    } on FormatException {
      return Failure(code: INVALID_FORMAT, errorResponse: "Invalid format");
    }
    catch (e) {
      return Failure(code: UNKNOW_ERROR, errorResponse: "Unknown Error");
    }
  }
}