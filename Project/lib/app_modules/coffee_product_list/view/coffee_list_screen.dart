import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_detail_screen/viewmodels/coffee_detail_viewmodel.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/model/coffee_list.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/viewmodels/coffee_viewmodel.dart';
import 'package:flutter_coding_challenge/app_modules/login_signup_screen/viewmodel/login_signup_viewmodel.dart';
import 'package:flutter_coding_challenge/utils/navigation_utils.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class PlaceholderWidget extends StatelessWidget {
  final Color color;
  PlaceholderWidget(this.color);

  @override
  Widget build(BuildContext context) {
    CoffeeViewModel coffeeListViewModel = context.watch<CoffeeViewModel>();
    LoginSingupViewModel loginSingupViewModel = context.watch<LoginSingupViewModel>();

    return _ui(coffeeListViewModel, context);
  }

  _ui(CoffeeViewModel coffeeViewModel, BuildContext context) {
    if (coffeeViewModel.loading) {
      return SpinKitRotatingCircle(
        color: Colors.blue,
        size: 50.0,
      );
    }
    return Padding(
    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
    child: GridView.count(
      crossAxisCount: 2,
      children: List.generate(
          coffeeViewModel.coffeeList.length,
              (index) => InkWell(
            onTap: () {
              Coffee coffee = coffeeViewModel.coffeeList[index];
              coffeeViewModel.setSelectedCoffee(coffee);
              navigateToCoffeeDetailScreen(context);
            },
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SelectCard(coffee: coffeeViewModel.coffeeList[index]),
              ),
              color: Colors.grey[200],
            ),
          )
      ),
    )
    );
  }
}

class SelectCard extends StatelessWidget {
  SelectCard({this.coffee});
  final Coffee? coffee;

  Widget getWidget(String? url) {
    if (url != null) {
      return FadeInImage.assetNetwork(
        placeholder: "images/placeholder.png",
        image: url,
        fit: BoxFit.fill,
      );
    } else {
      return Image.asset("images/placeholder.png");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        color: Colors.black,
        child: Column(
            children: <Widget>[
              Expanded(
                flex: 4,
                  child: Container(
                child: getWidget(coffee?.image),
              )),
              Expanded(
                flex: 1,
                  child:
              Text(coffee?.title ?? "")),
              Expanded(
                  flex: 1,
                  child:
                  Text("USD " + (coffee?.price.toString() ?? ""))
              )
            ]
        )
    );
  }
}
