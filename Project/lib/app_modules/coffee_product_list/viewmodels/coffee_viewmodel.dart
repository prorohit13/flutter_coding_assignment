import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/model/coffee_error.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/model/coffee_list.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/repos/api_status.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/repos/coffee_service.dart';

class CoffeeViewModel extends ChangeNotifier {

  bool _loading = false;
  bool get loading => _loading;
  setLoading(bool loading) async {
    _loading = loading;
    notifyListeners();
  }

  List<Coffee> _coffeeList = [];
  List<Coffee> get coffeeList => _coffeeList;
  setCoffeeListModel(List<Coffee> coffeeList) async {
    _coffeeList = coffeeList;
  }

  CoffeeError _coffeeError = CoffeeError(code: 0, errorResponse: null);
  CoffeeError get coffeeError => _coffeeError;
  setCoffeeError(CoffeeError coffeeError) async {
    _coffeeError = coffeeError;
  }

  Coffee _selectedCoffee = Coffee();
  Coffee get selectedCoffee => _selectedCoffee;
  setSelectedCoffee(Coffee coffee) async {
    _selectedCoffee = coffee;
  }

  CoffeeViewModel(){
    getCoffeeList();
  }

  getCoffeeList() async {
    setLoading(true);
    var response = await CoffeeService.getCoffeeList();
    if (response is Success) {
      setCoffeeListModel((response.response as CoffeeModel).data);
    } 
    if (response is Failure) {
      setCoffeeError(CoffeeError(code: response.code, errorResponse: response.errorResponse));
    }
    setLoading(false);
  }

}