// @dart=2.9
import 'dart:convert';

CoffeeModel coffeeFromJson(String str) => CoffeeModel.fromJson(json.decode(str));

String coffeeToJson(CoffeeModel data) => json.encode(data.toJson());

class CoffeeModel {
  CoffeeModel({
    this.data,
  });

  List<Coffee> data;

  factory CoffeeModel.fromJson(Map<String, dynamic> json) => CoffeeModel(
    data: List<Coffee>.from(json["data"].map((x) => Coffee.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Coffee {
  Coffee({
    this.id,
    this.title,
    this.price,
    this.description,
    this.image,
  });

  int id;
  String title;
  double price;
  String description;
  String image;

  factory Coffee.fromJson(Map<String, dynamic> json) => Coffee(
    id: json["id"],
    title: json["title"],
    price: json["price"].toDouble(),
    description: json["description"],
    image: json["image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "price": price,
    "description": description,
    "image": image,
  };
}
