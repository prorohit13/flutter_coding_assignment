class CoffeeError {
  int? code;
  Object? errorResponse;

  CoffeeError({this.code, this.errorResponse});
}

