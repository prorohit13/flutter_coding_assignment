import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/utils/app_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_coding_challenge/utils/app_constants.dart';

class SingupViewModel extends ChangeNotifier {

  String? validateConfirmPassword(String? password, String? confirmPassword) {
    if(confirmPassword?.isEmpty == true) {
      return '* Required';
    } else if ((confirmPassword?.length ?? "".length) < 6) {
      return "Password should be atleast 6 characters";
    } else if ((confirmPassword?.length ?? "".length) > 15) {
      return "Password should not be greater than 15 characters";
    } else if(confirmPassword != password) {
      return 'Password and confirm password does not match';
    } else {
      return null;
    }
  }

  void saveUsersInSharedPreference(Map<String, String> userMap) async {
    final prefs = await SharedPreferences.getInstance();
    var userJsonString = json.encode(userMap);
    prefs.setString(LOGGEDIN_USERS, userJsonString);
  }
}