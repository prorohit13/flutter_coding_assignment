
import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/app_modules/login_signup_screen/viewmodel/login_signup_viewmodel.dart';
import 'package:flutter_coding_challenge/app_modules/signup_screen/viewmodel/signup_viewmodel.dart';
import 'package:flutter_coding_challenge/components/button_widget.dart';
import 'package:flutter_coding_challenge/utils/navigation_utils.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:provider/src/provider.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _pass = TextEditingController();
  final TextEditingController _confirmPass = TextEditingController();

  @override
  Widget build(BuildContext context) {
    SingupViewModel singupViewModel = context.watch<SingupViewModel>();
    LoginSingupViewModel loginSingupViewModel = context.watch<LoginSingupViewModel>();

    return Scaffold(
      appBar: AppBar(
        title: Text("Singup"),
        backgroundColor: Colors.black,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.black,
          height: MediaQuery.of(context).size.height,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Form(
              autovalidate: true, //check for validation while typing
              key: formkey,
              child: Container(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 60.0),
                      child: ClipOval(
                        child: Container(
                          child: Image.asset("images/rest1.jpeg"),
                          height: 200, //MediaQuery.of(context).size.height / 2,
                          width: 200, //MediaQuery.of(context).size.width / 2,
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 40,
                    ),

                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: TextFormField(
                        controller: _email,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Email Id',
                              hintText: 'Enter valid email id as abc@gmail.com'),
                          validator: MultiValidator([
                            RequiredValidator(errorText: "* Required"),
                            EmailValidator(errorText: "Enter valid email id"),
                          ]
                          )
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 15.0, right: 15.0, top: 15, bottom: 0),
                      child: TextFormField(
                        controller: _pass,
                          obscureText: true,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Password',
                              hintText: 'Enter secure password'),
                          validator: MultiValidator([
                            RequiredValidator(errorText: "* Required"),
                            MinLengthValidator(6,
                                errorText: "Password should be atleast 6 characters"),
                            MaxLengthValidator(15,
                                errorText:
                                "Password should not be greater than 15 characters"),
                          ]),
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 15.0, right: 15.0, top: 15, bottom: 0),
                      child: TextFormField(
                         controller: _confirmPass,
                          obscureText: true,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Confirm Password',
                              hintText: 'Enter confirm password'),
                        validator: (value){
                          return singupViewModel.validateConfirmPassword(_pass.value.text, value);
                        },
                      ),
                    ),

                    SizedBox(
                        height: 32
                    ),

                    CustomButton("Signup", 48, 30.0, 1.0, Colors.blueAccent,
                        Colors.white, Colors.blueAccent, () async {
                          if (formkey.currentState?.validate() != false) {
                            Map<String, String> userCrdentialMap = {_email.value.text: _pass.value.text};
                            loginSingupViewModel.saveUsersLoginToSharedPreference(true);
                            singupViewModel.saveUsersInSharedPreference(userCrdentialMap);
                            navigateToHomeScreen(context);
                          }
                        }
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
