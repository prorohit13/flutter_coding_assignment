import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/components/button_widget.dart';
import 'package:flutter_coding_challenge/utils/navigation_utils.dart';
import 'package:flutter_coding_challenge/utils/singleton_utils.dart';
import 'package:flutter_coding_challenge/utils/snackbar_utils.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'cart_cell.dart';

class CartScreen extends StatefulWidget {
  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {

  bool checkoutInProgress = false;

  @override
  Widget build(BuildContext context) {
    return _ui(context);
  }

  Widget buildTransparent(BuildContext context) {
    return SpinKitRotatingCircle(
      color: Colors.blue,
      size: 50.0,
    );
  }

  _ui(BuildContext context) {
    return checkoutInProgress ? buildTransparent(context) : Padding(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: Singleton.instance.cartProducts.length == 0 ? Center(
          child: Container(
            child: Text("No products in cart"),
          ),
        ) : Column(
          children: [Expanded(
            flex: 9,
            child: GridView.count(
              crossAxisCount: 1,
              children: List.generate(
                  Singleton.instance.cartProducts.length,
                      (index) => InkWell(
                    onTap: () {
                    },
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: CartCellCard(coffee: Singleton.instance.cartProducts[index]),
                      ),
                      color: Colors.grey[200],
                    ),
                  )
              ),
            ),
          ),

            Expanded(
                flex: 1,
                child: Container(
                  color: Colors.white,
              child:  CustomButton("Checkout", 48, 8.0, 1.0, Colors.blueAccent,
                  Colors.white, Colors.blueAccent, () async {
                setState(() {
                  checkoutInProgress = true;
                });
                    Future.delayed(const Duration(milliseconds: 2000), () {
                      showSnackBar(context, "Congratulations! Order placed successfully");
                      Singleton.instance.cartProducts.clear();
                      navigateToHomeScreen(context);
                    });
                  }),
            )
            )
          ],
        )
    );
  }
}

