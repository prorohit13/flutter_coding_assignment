import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CartCellCard extends StatelessWidget {
  CartCellCard({this.coffee});
  final Map<String, dynamic>? coffee;

  Widget getWidget(String? url) {
    if (url != null) {
      return FadeInImage.assetNetwork(
        placeholder: "images/placeholder.png",
        image: url,
        fit: BoxFit.fill,
      );
    } else {
      return Image.asset("images/placeholder.png");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        color: Colors.black,
        child: Column(
            children: <Widget>[
              Container(
                child: Expanded(
                    flex: 4,
                    child: Container(
                      child: getWidget(coffee?["image"]),
                    )),
              ),
              Expanded(
                  flex: 1,
                  child:
                  Text(coffee?["title"] ?? "")),
              Expanded(
                  flex: 1,
                  child:
                  Text("USD " + (coffee?["price"].toString() ?? ""))
              )
            ]
        )
    );
  }
}