import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/app_modules/cart_screen/views/cart_cell.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/model/coffee_list.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/viewmodels/coffee_viewmodel.dart';
import 'package:flutter_coding_challenge/app_modules/login_signup_screen/viewmodel/login_signup_viewmodel.dart';
import 'package:flutter_coding_challenge/utils/singleton_utils.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class FavouritScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _ui(context);
  }

  _ui(BuildContext context) {
    return Padding(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: Singleton.instance.favCoffees.length == 0 ? Center(
          child: Container(
            child: Text("No favorite products found"),
          ),
        ) : GridView.count(
          crossAxisCount: 1,
          children: List.generate(
              Singleton.instance.favCoffees.length,
                  (index) => InkWell(
                onTap: () {
                },
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CartCellCard(coffee: Singleton.instance.favCoffees[index]),
                  ),
                  color: Colors.grey[200],
                ),
              )
          ),
        )
    );
  }
}