// import 'package:flutter/material.dart';
// import 'package:shared_preferences/shared_preferences.dart';
//
// class LoginPage extends StatefulWidget {
// @override
// _LoginPageState createState() => _LoginPageState();
// }
// class _LoginPageState extends State<LoginPage> {
//   // Create a text controller and use it to retrieve the current value
//   // of the TextField.
//   final username_controller = TextEditingController();
//   final password_controller = TextEditingController();
//   late SharedPreferences logindata;
//   late bool newuser;
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     check_if_already_login();
//   }
//   void check_if_already_login() async {
//     logindata = await SharedPreferences.getInstance();
//     newuser = (logindata.getBool('login') ?? true);
//   }
//   @override
//   void dispose() {
//     // Clean up the controller when the widget is disposed.
//     username_controller.dispose();
//     password_controller.dispose();
//     super.dispose();
//   }
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(" Shared Preferences"),
//       ),
//       body: Center(
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: <Widget>[
//             Text(
//               "Login Form",
//               style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
//             ),
//             Text(
//               "To show Example of Shared Preferences",
//               style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
//             ),
//             Padding(
//               padding: const EdgeInsets.all(15.0),
//               child: TextField(
//                 controller: username_controller,
//                 decoration: InputDecoration(
//                   border: OutlineInputBorder(),
//                   labelText: 'username',
//                 ),
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.all(15.0),
//               child: TextField(
//                 controller: password_controller,
//                 decoration: InputDecoration(
//                   border: OutlineInputBorder(),
//                   labelText: 'Password',
//                 ),
//               ),
//             ),
//             RaisedButton(
//               textColor: Colors.white,
//               color: Colors.blue,
//               onPressed: () {
//                 String username = username_controller.text;
//                 String password = password_controller.text;
//                 if (username != '' && password != '') {
//                   print('Successfull');
//                   logindata.setBool('login', false);
//                   logindata.setString('username', username);
//                   // Navigator.push(context,
//                   //     MaterialPageRoute(builder: (context) => MyDashboard()));
//                 }
//               },
//               child: Text("Log-In"),
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/app_modules/login_screen/login_viewmodel/login_viewmodel.dart';
import 'package:flutter_coding_challenge/app_modules/login_signup_screen/viewmodel/login_signup_viewmodel.dart';
import 'package:flutter_coding_challenge/components/button_widget.dart';
import 'package:flutter_coding_challenge/utils/navigation_utils.dart';
import 'package:flutter_coding_challenge/utils/snackbar_utils.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:provider/src/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final TextEditingController _emailId = TextEditingController();
  final TextEditingController _password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    LoginViewModel loginViewModel = context.watch<LoginViewModel>();
    LoginSingupViewModel loginSingupViewModel = context.watch<LoginSingupViewModel>();
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
        backgroundColor: Colors.black,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.black,
          height: MediaQuery.of(context).size.height,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Form(
              autovalidate: true, //check for validation while typing
              key: formkey,
              child: Container(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 60.0),
                      child: ClipOval(
                        child: Container(
                          child: Image.asset("images/rest1.jpeg"),
                          height: 200, //MediaQuery.of(context).size.height / 2,
                          width: 200, //MediaQuery.of(context).size.width / 2,
                        ),
                      ),
                    ),
                    SizedBox(height: 40,),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: TextFormField(
                        controller: _emailId,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Email Id',
                              hintText: 'Enter valid email id as abc@gmail.com'),
                          validator: MultiValidator([
                            RequiredValidator(errorText: "* Required"),
                            EmailValidator(errorText: "Enter valid email id"),
                          ])),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 15.0, right: 15.0, top: 15, bottom: 0),
                      child: TextFormField(
                        controller: _password,
                          obscureText: true,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Password',
                              hintText: 'Enter secure password'),
                          validator: MultiValidator([
                            RequiredValidator(errorText: "* Required"),
                            MinLengthValidator(6,
                                errorText: "Password should be atleast 6 characters"),
                            MaxLengthValidator(15,
                                errorText:
                                "Password should not be greater than 15 characters")
                          ])
                        //validatePassword,        //Function to check validation
                      ),
                    ),
                    SizedBox(height: 32),
                    CustomButton("Login", 48, 30.0, 1.0, Colors.blueAccent,
                        Colors.white, Colors.blueAccent, () async {
                          if (formkey.currentState?.validate() != false) {
                            var isValid = await loginViewModel.validateUserLogin(_emailId.value.text,
                                _password.value.text);
                            loginSingupViewModel.saveUsersLoginToSharedPreference(isValid);
                            if (isValid) {
                              navigateToHomeScreen(context);
                            } else {
                              showSnackBar(context, "Wrong email id or password");
                            }
                          }
                        }
                        ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

/*




* */