import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/utils/app_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginViewModel extends ChangeNotifier {
  Future<String?> getUserEmailFromSharedPreference() async {
    final prefs = await SharedPreferences.getInstance();
    var jsonString = prefs.getString(LOGGEDIN_USERS);
    if (jsonString == null) {
      return null;
    } else {
      var userMap = jsonDecode(jsonString) as Map<String, dynamic>;
      return userMap.keys.first as String;
    }
  }

  Future<String?> getUserPasswordFromSharedPreference() async {
    final prefs = await SharedPreferences.getInstance();
    var jsonString = prefs.getString(LOGGEDIN_USERS);
    if (jsonString == null) {
      return null;
    } else {
      var userMap = jsonDecode(jsonString) as Map<String, dynamic>;
      return userMap.values.first as String;
    }
  }

  Future<bool> validateUserLogin(String emailid, String password) async {
    var userStoredEmailId = await this.getUserEmailFromSharedPreference();
    var userStoredPassword = await this.getUserPasswordFromSharedPreference();
    if (userStoredEmailId == null || emailid != userStoredEmailId) {
      return false;
    }
    if (userStoredPassword == null || userStoredPassword != password) {
      return false;
    }
    return true;
  }

}



