import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/app_modules/cart_screen/views/cart_screen.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/view/coffee_list_screen.dart';
import 'package:flutter_coding_challenge/app_modules/fav_screen/views/favorite_screen.dart';
import 'package:flutter_coding_challenge/app_modules/login_signup_screen/view/login_signup_screen.dart';
import 'package:flutter_coding_challenge/app_modules/login_signup_screen/viewmodel/login_signup_viewmodel.dart';
import 'package:flutter_coding_challenge/components/logout_button.dart';
import 'package:flutter_coding_challenge/utils/app_constants.dart';
import 'package:flutter_coding_challenge/utils/navigation_utils.dart';
import 'package:provider/src/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State {
  int _currentIndex = 0;
  final List _children = [
    PlaceholderWidget(Colors.black),
    FavouritScreen(),
    CartScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('My Coffee!'),
        actions: <Widget>[
          LogoutButton(),
        ],
      ),
      body: _children[_currentIndex], // new
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.black,
        onTap: onTabTapped, // new
        currentIndex: _currentIndex, // new
        items: [
          const BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
          const BottomNavigationBarItem(
              icon: Icon(Icons.favorite), label: "Favorites"),
          const BottomNavigationBarItem(icon: Icon(Icons.shop), label: "Cart")
        ],
      ),
    );
  }

  void onTabTapped(int index)  {
    setState(()  {
      _currentIndex = index;
    });
  }
}
