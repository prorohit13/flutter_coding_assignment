import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_detail_screen/viewmodels/coffee_detail_viewmodel.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/viewmodels/coffee_viewmodel.dart';
import 'package:flutter_coding_challenge/app_modules/login_signup_screen/viewmodel/login_signup_viewmodel.dart';
import 'package:flutter_coding_challenge/components/logout_button.dart';
import 'package:flutter_coding_challenge/utils/navigation_utils.dart';
import 'package:flutter_coding_challenge/utils/singleton_utils.dart';
import 'package:provider/src/provider.dart';

class CoffeeDetailScreen extends StatefulWidget {

  @override
  State<CoffeeDetailScreen> createState() => _CoffeeDetailScreenState();
}

class _CoffeeDetailScreenState extends State<CoffeeDetailScreen> {

  bool isAlreadySaved = false;
  bool isAlreadyInCart = false;

  @override
  Widget build(BuildContext context) {

    CoffeeViewModel coffeeViewModel = context.watch<CoffeeViewModel>();
    LoginSingupViewModel loginSingupViewModel = context.watch<LoginSingupViewModel>();
    CoffeeDetailViewModel coffeeDetailViewModel = context.watch<CoffeeDetailViewModel>();

    Widget getImageWidget(String? url) {
      if (url != null) {
        return FadeInImage.assetNetwork(
          placeholder: "images/placeholder.png",
          image: url,
          fit: BoxFit.fitHeight,
        );
      } else {
        return Image.asset("images/placeholder.png");
      }
    }

    void handleFavoriteAction() async {
      var isLoggedIn = await loginSingupViewModel.getUsersLoginFromSharedPreference();
      if(isLoggedIn == false) {
        navigateToLoginSignupScreen(context, true);
        return;
      }

      if(coffeeDetailViewModel.isCoffeeAlreadyFav(coffeeViewModel.selectedCoffee) == true) {
        setState(() {
          this.isAlreadySaved = false;
        });
        coffeeDetailViewModel.removeFromFavCoffeeList(coffeeViewModel.selectedCoffee);
      } else {
        setState(() {
          this.isAlreadySaved = true;
        });
        coffeeDetailViewModel.addToFavCoffeeList(coffeeViewModel.selectedCoffee);
      }
    }

    void handleAddToCartAction() async {
      var isLoggedIn = await loginSingupViewModel.getUsersLoginFromSharedPreference();
      if(isLoggedIn == false) {
        navigateToLoginSignupScreen(context, true);
        return;
      }

      if(coffeeDetailViewModel.isCoffeeAlreadyAddedToCart(coffeeViewModel.selectedCoffee) == true) {
        setState(() {
          this.isAlreadyInCart = false;
        });
        coffeeDetailViewModel.removeFromCartList(coffeeViewModel.selectedCoffee);
      } else {
        setState(() {
          this.isAlreadyInCart = true;
        });
        coffeeDetailViewModel.addToCartList(coffeeViewModel.selectedCoffee);
      }
    }

    Widget getMobileView() {
      return Column(
        children: [
          Expanded(
            flex: 9,
            child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                      child: Container(
                        height: MediaQuery.of(context).size.width / 1.5,
                        child: getImageWidget(coffeeViewModel.selectedCoffee.image),
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                coffeeViewModel.selectedCoffee.title,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Row(
                                children: [
                                  Text(
                                    "USD" +
                                        " " +
                                        (coffeeViewModel.selectedCoffee.price?.toString() ??
                                            ""),
                                    textAlign: TextAlign.end,
                                    style: TextStyle(
                                      fontSize: 24,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),

                                  SizedBox(width: 16,),
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.blueAccent,
                                    ),
                                    onPressed: () {
                                      handleFavoriteAction();
                                    },
                                    child: Text(
                                      coffeeDetailViewModel.isCoffeeAlreadyFav(coffeeViewModel.selectedCoffee) == false ? "Add To Fav" : "Already Fav",
                                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                coffeeViewModel.selectedCoffee.description,
                                style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
          ),
          Expanded(
            flex: 1,
            child: Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                child: Container(
                  width: double.infinity,
                  height: 50,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.blueAccent,
                    ),
                    onPressed: () {
                      handleAddToCartAction();
                    },
                    child: Text(
      coffeeDetailViewModel.isCoffeeAlreadyAddedToCart(coffeeViewModel.selectedCoffee) == false ? "Add To Cart" : "In Cart",
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          coffeeViewModel.selectedCoffee.title,
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Colors.black,
        elevation: 0.0,
        leading: BackButton(color: Colors.white),
        actions: <Widget>[
         LogoutButton(),
        ],

      ),
      body: getMobileView(),
    );
  }
}
