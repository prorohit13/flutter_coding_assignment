
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/model/coffee_list.dart';
import 'package:flutter_coding_challenge/utils/app_constants.dart';
import 'package:flutter_coding_challenge/utils/singleton_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CoffeeDetailViewModel extends ChangeNotifier {

  void addToFavCoffeeList(Coffee coffee) {
    Map<String, String> map = {};
    map["id"] = coffee.id.toString();
    map["title"] = coffee.title;
    map["image"] = coffee.image;
    map["price"] = coffee.price.toString();
    Singleton.instance.favCoffees.add(map);
    this.saveFavCoffeesInSharedPreference();
  }

  void removeFromFavCoffeeList(Coffee coffee) {
    Singleton.instance.favCoffees.forEach((element) {
      var id = element["id"];
      if (id == coffee.id.toString()) {
        Singleton.instance.favCoffees.remove(element);
      }
    });
  }

  bool? isCoffeeAlreadyFav(Coffee coffee) {
    var result = false;
    for (var element in Singleton.instance.favCoffees) {
        var id = element["id"];
        if (id == coffee.id.toString()) {
          result = true;
          break;
        }
    }
    return result;
  }

  void saveFavCoffeesInSharedPreference() async {
    final prefs = await SharedPreferences.getInstance();
    var jsonString = json.encode(Singleton.instance.favCoffees);
    prefs.setString(FAV_COFFEES, jsonString);
  }

  void addToCartList(Coffee coffee) {
    Map<String, String> map = {};
    map["id"] = coffee.id.toString();
    map["title"] = coffee.title;
    map["image"] = coffee.image;
    map["price"] = coffee.price.toString();
    Singleton.instance.cartProducts.add(map);
    this.saveCoffeeInSharedPreferenceCartList();
  }

  void saveCoffeeInSharedPreferenceCartList() async {
    final prefs = await SharedPreferences.getInstance();
    var jsonString = json.encode(Singleton.instance.cartProducts);
    prefs.setString(FAV_COFFEES, jsonString);
  }

  void removeFromCartList(Coffee coffee) {
    Singleton.instance.cartProducts.forEach((element) {
      var id = element["id"];
      if (id == coffee.id.toString()) {
        Singleton.instance.cartProducts.remove(element);
      }
    });
  }

  bool? isCoffeeAlreadyAddedToCart(Coffee coffee) {
    var result = false;
    for (var element in Singleton.instance.cartProducts) {
      var id = element["id"];
      if (id == coffee.id.toString()) {
        result = true;
        break;
      }
    }
    return result;
  }
}