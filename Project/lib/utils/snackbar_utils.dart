import 'package:flutter/material.dart';

void showSnackBar(BuildContext context, String message) {
  ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(message,
        textAlign: TextAlign.center, style:
        TextStyle(fontSize: 16.0, color: Colors.white,
            fontWeight:
        FontWeight.bold),
      ),
        duration: Duration(seconds: 2),
        backgroundColor: Colors.blueAccent,)
  );
}