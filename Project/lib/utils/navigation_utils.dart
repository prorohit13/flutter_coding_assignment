import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_detail_screen/views/coffee_detail_screen.dart';
import 'package:flutter_coding_challenge/app_modules/login_screen/views/login_screen.dart';
import 'package:flutter_coding_challenge/app_modules/login_signup_screen/view/login_signup_screen.dart';
import 'package:flutter_coding_challenge/app_modules/signup_screen/views/signup_screen.dart';
import '../app_modules/home/home_screen.dart';

void navigateToHomeScreen(BuildContext context) async {
  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>
      HomeScreen()),);
}

void navigateToLoginSignupScreen(BuildContext context, bool shouldReturnToLastScreen) async {
  if (shouldReturnToLastScreen) {
    Navigator.push(context, MaterialPageRoute(builder: (context) =>
        LoginSignupScreen()),);
  } else {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => LoginSignupScreen()),
          (Route<dynamic> route) => false,
    );
  }
}

void navigateToLoginScreen(BuildContext context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) =>
      LoginPage()),);
}

void navigateToSignupScreen(BuildContext context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) =>
      SignupScreen()),);
}

void navigateToCoffeeDetailScreen(BuildContext context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) =>
      CoffeeDetailScreen()),);
}



