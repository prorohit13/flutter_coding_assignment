
const String COFFEE_LIST_URL = "https://608556cdd14a870017577b22.mockapi.io/mftg/coffeelist";
const String ALREADY_LOGIN = "already_login";
const String LOGGEDIN_USERS = "loggedin_users";
const String FAV_COFFEES = "FAV_COFFEES";


const INVALID_RESPONSE = 100;
const NO_INTERNET = 101;
const INVALID_FORMAT = 102;
const UNKNOW_ERROR = 103;

