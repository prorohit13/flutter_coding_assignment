import 'dart:convert';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/model/coffee_list.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app_constants.dart';

class Singleton {
  static Singleton _instance = Singleton._();
  Singleton._();
  static Singleton get instance => _instance;

  List<Map<String, dynamic>> favCoffees = [];
  List<Map<String, dynamic>> cartProducts = [];
}