// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/app_modules/coffee_product_list/viewmodels/coffee_viewmodel.dart';
import 'package:flutter_coding_challenge/app_modules/home/home_screen.dart';
import 'package:flutter_coding_challenge/app_modules/login_signup_screen/viewmodel/login_signup_viewmodel.dart';
import 'package:flutter_coding_challenge/utils/app_constants.dart';
import 'package:flutter_coding_challenge/utils/singleton_utils.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'app_modules/coffee_detail_screen/viewmodels/coffee_detail_viewmodel.dart';
import 'app_modules/login_screen/login_viewmodel/login_viewmodel.dart';
import 'app_modules/login_signup_screen/view/login_signup_screen.dart';
import 'app_modules/signup_screen/viewmodel/signup_viewmodel.dart';
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool login = prefs.getBool(ALREADY_LOGIN);
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create: (_) => CoffeeViewModel()),
    ChangeNotifierProvider(create: (_) => SingupViewModel()),
    ChangeNotifierProvider(create: (_) => LoginViewModel()),
    ChangeNotifierProvider(create: (_) => LoginSingupViewModel()),
    ChangeNotifierProvider(create: (_) => CoffeeDetailViewModel())
  ], child: MaterialApp(
    home: login == null || login == false ? LoginSignupScreen() : HomeScreen(),
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      brightness: Brightness.dark,
    ),
  )
  )
  );
}