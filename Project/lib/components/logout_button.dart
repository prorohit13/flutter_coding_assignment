import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/app_modules/login_signup_screen/viewmodel/login_signup_viewmodel.dart';
import 'package:flutter_coding_challenge/utils/navigation_utils.dart';
import 'package:flutter_coding_challenge/utils/singleton_utils.dart';
import 'package:provider/src/provider.dart';

import 'alert_widget.dart';

class LogoutButton extends StatelessWidget {
  const LogoutButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    LoginSingupViewModel loginSingupViewModel = context.watch<LoginSingupViewModel>();

    return  Padding(
        padding: EdgeInsets.only(right: 20.0),
        child: GestureDetector(
          onTap: () async {
            showAlertDialog(context,
                "Logout",
                "Are you sure to logout?",
                "Yes",
                "No", () async {
                  loginSingupViewModel.saveUsersLoginToSharedPreference(false);
                  Singleton.instance.favCoffees.clear();
                  Singleton.instance.cartProducts.clear();
                  navigateToLoginSignupScreen(context, false);
              }, () {
            });

          },
          child: Icon(
            Icons.logout,
            size: 26.0,
          ),
        )
    );
  }
}
