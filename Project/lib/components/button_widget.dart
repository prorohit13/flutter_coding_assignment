import 'package:flutter/material.dart';
class CustomButton extends StatelessWidget {
  final String name;
  final double cornerRadius, borderWidth, buttonHeight;
  final Color buttonBackgroundColor, buttonTextColor, borderColor;
  final Function onPressed;
  const CustomButton (this.name,
      this.buttonHeight,
      this.cornerRadius,
      this.borderWidth,
      this.buttonBackgroundColor,
      this.buttonTextColor,
      this.borderColor,
      this.onPressed);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(16, 4, 16, 16),
      child: Container(
        height: buttonHeight,
        child: GestureDetector(
          onTap: () {
            onPressed();
          },
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: borderColor,
                style: BorderStyle.solid,
                width: borderWidth,
              ),
              color: buttonBackgroundColor,
              borderRadius: BorderRadius.circular(cornerRadius),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Text(
                    name,
                    style: TextStyle(
                      decoration: TextDecoration.none,
                      color: buttonTextColor,
                      fontWeight: FontWeight.w400,
                      fontSize: 20,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
