import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

showAlertDialog(BuildContext context,
    String title,
    String subtitle,
    String yesTitle,
    String noTitle,
    Function yesPressed,
    Function noPressed
) {

  // set up the buttons
  Widget cancelButton = TextButton(
    child: Text(noTitle),
    onPressed:  () {
      Navigator.of(context).pop();
      noPressed();
    },
  );
  Widget continueButton = TextButton(
    child: Text(yesTitle),
    onPressed:  () {
      yesPressed();
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(subtitle),
    actions: [
      cancelButton,
      continueButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}